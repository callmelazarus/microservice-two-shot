import json
from django.shortcuts import render

from django.views.decorators.http import require_http_methods
from django.http import JsonResponse

from common.json import ModelEncoder

from hats_rest.models import Hat, LocationVO


class LocationVOEncoder(ModelEncoder):
  model = LocationVO
  properties = [
    "import_href",
    "closet_name"
  ]

class HatListEncoder(ModelEncoder):
  model = Hat
  properties =[
    "id",
    "style",
    "color",
  ]

class HatDetailEncoder(ModelEncoder):
  model = Hat
  properties =[
    "id",
    "fabric",
    "style",
    "color",
    "picture_url",
    "location"
  ]
  # set up this encoder to encode the Location object into JSON
  encoders = {
    "location": LocationVOEncoder(),
  }


# GET list, or POST single hat
@require_http_methods(["GET", "POST"])
def api_list_hats(request):
  if request.method == "GET":
    hats = Hat.objects.all()
    # print(hats, '-----')
    return JsonResponse(
      {"hat": hats},
      encoder=HatListEncoder
    )
  else: # POST
    content = json.loads(request.body)
    # print(content)
    # retrive the location information
    try:
        location_href = content["location"]
        location = LocationVO.objects.get(import_href=location_href)
        content["location"] = location
    except LocationVO.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid location id"},
            status=400,
        )
    hat = Hat.objects.create(**content)
    return JsonResponse(
      hat,
      encoder=HatDetailEncoder,
      safe=False
    )

# GET DETAIL, or DELETE single hat
@require_http_methods(["GET", "DELETE"])
def api_show_hats(request, pk):
  if request.method == "GET":
    hat = Hat.objects.get(id=pk)
    return JsonResponse(
      hat,
      encoder=HatDetailEncoder,
      safe=False
    )
  else: # DELETE
      count, _ = Hat.objects.filter(id=pk).delete()
      return JsonResponse({"deleted": count > 0})
