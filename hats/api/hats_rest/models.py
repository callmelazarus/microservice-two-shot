from django.db import models
from django.urls import reverse


# what attributes should we make for LocationVO? Deciding to note href, and name
class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)


class Hat(models.Model):
  fabric = models.CharField(max_length=100)
  style = models.CharField(max_length=100)
  color = models.CharField(max_length=40)
  picture_url=models.URLField(max_length=300)
  # many hats to one location
  # when a LOCATION is deleted -> delete ALL hates (CASCADE) 
  location=models.ForeignKey(
    LocationVO,
    related_name="hats",
    on_delete=models.CASCADE,
    )
  # what is this doing ?? 
  # need to check first argument in reverse
  def get_api_url(self):
      return reverse("api_show_hats", kwargs={"pk": self.pk})