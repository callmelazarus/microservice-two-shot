from django.urls import path

from hats_rest.views import (
    api_list_hats, api_show_hats
)

# collection of hats
# get list and post single hats
# localhost:8000/api/hats/

# single hat
# GET, PUT, and DELETE single hat
# /api/hats/<int:pk>/

urlpatterns = [
   path("hats", api_list_hats, name="api_list_hats"),
   path("hats/<int:pk>/", api_show_hats, name="api_show_hats")
]
