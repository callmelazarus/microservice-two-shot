import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

# Import models from hats_rest, here.
# from hats_rest.models import Something
from hats_rest.models import LocationVO

# added function to retrieve location info
def get_locations():
    location_url = "http://wardrobe-api:8000/api/locations/"
    response = requests.get(location_url)
    content = json.loads(response.content)
    # print("content:", content)

    # loop thru the various locations
    # update or create a LocationVO instance if a new location exists
    # assign import_href attribute with the href attribute from Locations
    for location in content["locations"]:
        LocationVO.objects.update_or_create(
            import_href=location["href"],
            defaults={"closet_name":location["closet_name"]},
        )

def poll():
    while True:
        print('Hats poller polling for data - test')
        try:
            print('inside try statement')
            get_locations()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
