# Wardrobify

Team:

- Person 1 - Michael: Shoes
- Person 2 - Jay: Hats

## Design

## Shoes microservice - Michael

Explain your models and integration with the wardrobe
microservice, here

The Shoe resource should track its manufacturer, its model name, its color,
a URL for a picture, and the bin in the wardrobe where it exists.

You must create RESTful APIs to get a list of shoes, create a new shoe, and delete a shoe.
You must create React component(s) to show a list of all shoes and their details.
You must create React component(s) to show a form to create a new shoe.
You must provide a way to delete a shoe.
You must route the existing navigation links to your components.

## Hats microservice - Jay

Accessible via Port 8090

Wardrobe API Port 8100 -> tap into Location for the wardrobe

Explain your models and integration with the wardrobe
microservice, here.

1. Start by creating models in hats/api/hats_rest/models.py. 
   1. Hat model and LocationVO model are required
2. Setup API's in hats/api/hats_rest/views.py (GET list, POST detail, GET detail, DELETE detail)
   1. Encoders will be required to encode the querysets objects into JSON
3. Setup Poller.py -> get location data from Wardrobe container. Update or create a LocationVO object based on wardrobe database.
   

Your Django application has a common directory that contains a json.py file that has a ModelEncoder in it, if you want to use it.

Hat Model attributes:
fabric
style name
color
URL for a picture
location in the wardrobe where it exists

### API directory
hats/api is a Django application with a Django project and a Django app already created. The Django app is not installed in the Django project. There are no URLs, views, or models yet created. You must create these.

### POLL directory
hats/poll is a polling application that uses the Django resources in the RESTful API project. It contains a script file hats/poll/poller.py that you must implement to pull Location data from the Wardrobe API.


create RESTful APIs to get a list of hats, create a new hat, and delete a hat.
create React component(s) to show a list of all hats and their details.
create React component(s) to show a form to create a new hat, delete a hat
route the existing navigation links to your components.

