from django.urls import path
from shoes_rest.views import shoe_list, shoe_detail

urlpatterns = [
    path("shoes/", shoe_list, name = "shoe_list"),
    path("shoes/<int:pk>/", shoe_detail, name= "shoe_detail")
]
