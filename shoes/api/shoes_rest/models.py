from django.db import models

# Create your models here.


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=50)
    model_name = models.CharField(max_length=50)
    color = models.CharField(max_length=50)
    picture_url = models.URLField(max_length=300)
    bin = models.ForeignKey("BinVO", related_name="Shoes",on_delete=models.CASCADE, null=True)

    


class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()
    import_href=models.CharField(max_length=200,unique=True)




        
     


