import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


async function loadShoes() {
  const shoesResponse = await fetch('http://localhost:8080/api/shoes/');

  if (shoesResponse.ok) {
    const shoeData = await shoesResponse.json();
    console.log("8888888888",shoeData)
    root.render(
      <React.StrictMode>
        <App shoes={shoeData.shoes} />
      </React.StrictMode>
    );
  } else {
    console.error(shoesResponse);
  }
}

loadShoes() ;

async function loadHats() {
  const hatResponse = await fetch('http://localhost:8090/api/hats');
  // console.log(response);
  if (hatResponse.ok) {
    const hatData = await hatResponse.json()
    // console.log("hatData", hatData)
    // console.log("hatData.hat", hatData.hat)
    root.render(
      <React.StrictMode>
        <App hatlist={hatData.hat} />
      </React.StrictMode>
    );
  } else {
    console.error("hi michael", hatResponse)
  }
}
loadHats();
