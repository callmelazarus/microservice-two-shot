import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ListofHats from "./hat";
import HatForm from "./hatform";
import HatDetail from "./hatdetail";
import ShoeList from './shoe';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path ="shoes">
            <Route path= "" element={<ShoeList shoes={props.shoes} />} />
          </Route>
          <Route path="/hats">
            <Route path="" element={<ListofHats myhats={props.hatlist} />} />
            <Route path="new" element={<HatForm />} />
            <Route path="detail" element={<HatDetail/>} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}
export default App;
