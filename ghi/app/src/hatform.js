import React from "react";
import { renderMatches } from "react-router-dom";

class HatForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fabric: "",
      style: "",
      color: "",
      pictureUrl: "",
      locations: [],
    };
    this.handleChangeFabric = this.handleChangeFabric.bind(this);
    this.handleChangeStyle = this.handleChangeStyle.bind(this);
    this.handleChangeColor = this.handleChangeColor.bind(this);
    this.handleChangePictureUrl = this.handleChangePictureUrl.bind(this);
    this.handleChangeLocation = this.handleChangeLocation.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  // retreive location information

  async componentDidMount() {
    const url = "http://localhost:8100/api/locations/";
    const response = await fetch(url);
    // console.log("response", response)

    if (response.ok) {
      const data = await response.json();
      // console.log('data:', data)
      this.setState({ locations: data.locations });
    } else {
      console.log("wardrobe location fetch repsonse no good");
    }
  }

  // handle state changes

  handleChangeFabric(event) {
    const value = event.target.value;
    this.setState({ fabric: value });
  }
  handleChangeStyle(event) {
    const value = event.target.value;
    this.setState({ style: value });
  }
  handleChangeColor(event) {
    const value = event.target.value;
    this.setState({ color: value });
  }
  handleChangePictureUrl(event) {
    const value = event.target.value;
    this.setState({ pictureUrl: value });
  }
  handleChangeLocation(event) {
    const value = event.target.value;
    this.setState({ location: value });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state }
    data.picture_url = data.pictureUrl
    delete data.pictureUrl
    delete data.locations;

    const hatUrl = "http://localhost:8090/api/hats";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {
      const newHat = await response.json();
      console.log("newhat", newHat);
      this.setState({
        fabric: "",
        style: "",
        color: "",
        pictureUrl: "",
      });
    }
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Hat</h1>
            <form onSubmit={this.handleSubmit}id="create-hat-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleChangeFabric}
                  placeholder="fabric"
                  required
                  type="text"
                  name="fabric"
                  id="fabric"
                  className="form-control"
                />
                <label htmlFor="fabric">Fabric Type</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleChangeStyle}
                  placeholder="style"
                  required
                  type="text"
                  name="style"
                  id="style"
                  className="form-control"
                />
                <label htmlFor="style">Hat Style</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleChangeColor}
                  placeholder="color"
                  required
                  type="text"
                  name="color"
                  id="color"
                  className="form-control"
                />
                <label htmlFor="color">Hat Color</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleChangePictureUrl}
                  placeholder="pictureUrl"
                  required
                  type="text"
                  name="pictureUrl"
                  id="pictureUrl"
                  className="form-control"
                />
                <label htmlFor="pictureUrl">Picture Url</label>
              </div>
              <div className="mb-3">
                <select
                  onChange={this.handleChangeLocation}
                  required
                  name="location"
                  id="location"
                  className="form-select"
                >
                  <option value="">Choose a location</option>
                  {this.state.locations.map((location) => {
                    return (
                      <option key={location.href} value={location.href}>
                        {location.closet_name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default HatForm;
