// fetch list, show list of hats
// bootstraps is installed

import React from "react";
import { NavLink } from "react-router-dom";


function HatDetail(props) {
  return (
    <>
    <h1> hi</h1>
      <h1 class="mt-5 mb-3 text-center">Detail page</h1>
      <table className="table table-striped table-light">
        <thead className="thead-light">
          <tr>
            <th>Hat</th>
            <th>Fabric</th>
            <th>Style</th>
            <th>Color</th>
          </tr>
        </thead>
        <tbody>
          {console.log("props.myhats", props.myhats, "----------------")}
          {props.myhats.map((hat) => {
            return (
              <tr key={hat.href}>
                <td>{hat.id}</td>
                <td>{hat.Fabric}</td>
                <td>{hat.Style}</td>
                <td>{hat.Color}</td>
                <td>
                  <button className="btn btn-outline-primary">
                    Hat {hat.id} details
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <NavLink to="/hats/">
        <button className="btn btn-primary">Back to List</button>
      </NavLink>
        <button className="btn btn-danger">Delete</button>
    </>
  );
}

export default HatDetail;
