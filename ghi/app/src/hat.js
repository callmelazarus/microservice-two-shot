// fetch list, show list of hats
// bootstraps is installed

import React from "react";
import { NavLink } from "react-router-dom";

function ListofHats(props) {
  async function handleClick(hatId) {
    const url = `http://localhost:8090/api/hats/${hatId}`;
    // console.log('url', url)
    const deleteId = JSON.stringify(hatId);
    const fetchConfig = {
      method: "delete",
      body: deleteId,
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfig);
    console.log(response);
  }

  return (
    <>
      <h1 class="mt-5 mb-3 text-center">Hats in your wardrobe</h1>
      <table className="table table-striped table-light">
        <thead className="thead-light">
          <tr>
            <th>Hat</th>
            <th>Style</th>
            <th>Color</th>
            <th>See detail (not working)</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {/* {console.log("props.myhats", props.myhats, "----------------")} */}
          {props.myhats.map((hat) => {
            return (
              <tr key={hat.href}>
                <td>{hat.id}</td>
                <td>{hat.style}</td>
                <td>{hat.color}</td>
                <td>
                  <NavLink to="/hat/detail">
                    <button className="btn btn-outline-primary">
                      Hat {hat.id} details
                    </button>
                  </NavLink>
                </td>
                <td>
                  <button
                    onClick={() => handleClick(hat.id)} // use anon function to pass arguments to custom function
                    className="btn btn-outline-danger"
                  >
                    Delete Hat {hat.id}
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <NavLink to="/hats/new/">
        <button className="btn btn-primary">Add a New Hat</button>
      </NavLink>
    </>
  );
}

export default ListofHats;
