
import React from "react";


function ShoeList(props) {
    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Model Name</th>
            <th>Color</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
        {console.log(props.shoes)}
        {props.shoes.map(shoe => {
                        return (                           
                            <tr key={shoe.id}>
                                <td>{shoe.model_name}</td>
                                <td>{shoe.color}</td>
                                <td>{shoe.manufacturer}</td>
                                <td>{shoe.picture_url}</td>  
                            </tr>
                        );
                    })}

        </tbody>
      </table>
    
    );
  }
  
  export default ShoeList;
